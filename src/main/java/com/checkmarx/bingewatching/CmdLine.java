package com.checkmarx.bingewatching;

import com.checkmarx.bingewatching.entities.Content;
import com.checkmarx.bingewatching.enums.ContentType;
import org.apache.commons.lang3.math.NumberUtils;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.Scanner;

import static java.lang.Thread.sleep;

// CmdLine class in in charge of the communication between the user and the application.
public class CmdLine {

    private final static String USER_MESSAGES_FILE_NAME = "userMessages.config";
    private Controller controller;
    private final static long SLEEP_TIME = 10000; // in milliseconds
    private Scanner sc = new Scanner(System.in);
    private Properties userMessages = new Properties();

    public static CmdLine object;

    private CmdLine() {}

    private CmdLine(Controller controller) throws IOException {
        InputStream inputStream =
                CmdLine.class.getClassLoader().getResourceAsStream(USER_MESSAGES_FILE_NAME);
        userMessages.load(inputStream);
        this.controller = controller;
    }

    public static CmdLine getObject(Controller controller) throws IOException {
        if (object == null) {
            object = new CmdLine(controller);
        }
        return object;
    }

    public void switchUser() {
        while(true) {
            System.out.println(userMessages.getProperty("switchUser"));
            String userId = sc.nextLine().trim();
            if (NumberUtils.isDigits(userId)) {
                controller.setCurrentUserId(userId);
                return;
            } else {
                System.out.println(userMessages.getProperty("invalidUserId"));
            }
        }
    }

    public void getContent() throws InterruptedException {
        while(true) {
            System.out.println(userMessages.getProperty("contentType"));
            String contentType = sc.nextLine().trim();
            ContentType selectedContentType;
            switch (contentType) {
                case "1":
                    selectedContentType = ContentType.TV_SHOW;
                    break;
                case "2":
                    selectedContentType = ContentType.MOVIE;
                    break;
                case "3":
                    selectedContentType = ContentType.ANY;
                    break;
                default:
                    System.out.println(userMessages.getProperty("invalidContent"));
                    continue;
            }
            JSONObject response = controller.getContent(selectedContentType);
            if (response == null) {
                System.out.println(userMessages.getProperty("serverError"));
                return;
            }
            printContent(response);
            waitDuringUserWatching();
            return;
        }
    }

    private void printContent(JSONObject contentObj) {
        System.out.println("title: " + contentObj.getString("title"));
        System.out.println("overview: " + contentObj.getString("overview"));
        System.out.println("released on: " + contentObj.getString("released_on"));
        System.out.println("imdb_rating: " + contentObj.getDouble("imdb_rating") + "\n");
    }

    private void waitDuringUserWatching() throws InterruptedException {
        while(true) {
            sleep(SLEEP_TIME);
            System.out.println(userMessages.getProperty("isWatchingFinished"));
            String answer = sc.nextLine().trim().toLowerCase();
            if (answer.equals("y")) {
                takeUserRank();
                return;
            }
        }
    }

    private void takeUserRank() {
        while(true) {
            System.out.println(userMessages.getProperty("rankContentRequest"));
            String answer = sc.nextLine().trim().toLowerCase();
            if (!(NumberUtils.isDigits(answer) && Integer.parseInt(answer) >= 0 && Integer.parseInt(answer) <= 10)) {
                System.out.println(userMessages.getProperty("invalidRank"));
            } else {
                this.controller.setUserRanking(Integer.parseInt(answer));
                break;
            }
        }
    }

    public int chooseOption() throws InterruptedException {
        System.out.println(userMessages.getProperty("chooseOption"));
        String userOption = sc.nextLine().trim().toLowerCase();
        switch (userOption) {
            case "s":
                switchUser();
                return 1;
            case "h":
                showUserHistory();
                return 1;
            case "c":
                getContent();
                return 1;
            case "e":
                return 0;
            default:
                System.out.println(userMessages.getProperty("invalidOption"));
                return 1;
        }
    }

    private void showUserHistory() {
        this.controller.getHistory().stream().map(Content::toString).forEach(System.out::println);
    }

    public void run() throws InterruptedException {
        System.out.println(userMessages.getProperty("welcome"));
        switchUser();
        int keepLoop = 1;
        while(keepLoop == 1) {
            keepLoop = chooseOption();
        }
        System.out.println(userMessages.getProperty("bye"));
    }
}
