package com.checkmarx.bingewatching;

import com.checkmarx.bingewatching.entities.Content;
import com.checkmarx.bingewatching.entities.User;
import com.checkmarx.bingewatching.enums.ContentType;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


public class Controller {

    public static final String URL_FORMATTER = "https://api.reelgood.com/v1/roulette/netflix?nocache=true&content_kind=%s&availability=onAnySource";

    private User currentUser;
    private Content currentContent;
    private Data data;
    private static Controller object;

    private Controller() {}

    private Controller(Data data) {
        this.data = data;
    }

    public static Controller getObject(Data data) {
        if (object == null) {
            object = new Controller(data);
        }
        return object;
    }

    public boolean setCurrentUserId(String newUserId) {
        Optional<User> newUserOptional = this.data.getUserList().stream().filter(user -> user.getUserId()
                .equals(newUserId)).findFirst();
        if (newUserOptional.isPresent()) {
            this.currentUser = newUserOptional.get();
            return true;
        } else {
            User newUser = new User(newUserId);
            this.data.addNewUser(newUser);
            this.currentUser = newUser;
            return false;
        }
    }

    public List<Content> getHistory() {
        return this.currentUser.getContents();
    }

    public JSONObject getContent(ContentType selectedContentType) {
        try {
            String contentKind = "";
            switch (selectedContentType) {
                case TV_SHOW:
                    contentKind = "show";
                    break;
                case MOVIE:
                    contentKind = "movie";
                    break;
                case ANY:
                    contentKind = "both";
                    break;
            }
            return readDataFromServer(contentKind);
        } catch (IOException e) {
            System.err.println("Cannot read data from server");
            System.err.println(e);
            return null;
        }
    }

    private JSONObject readDataFromServer(String contentKind) throws IOException {
        while(true) {
            URL url = new URL(String.format(URL_FORMATTER, contentKind));
            JSONObject response;
            try {
                response = getResponseFromServer(url);
            } catch (IOException e) {
                return null;
            }
            // if 'handleServerResponse' returns false, it means that the server retrieved a content that the user already watched
            if (handleServerResponse(response)) {
                return response;
            }
        }
    }

    private JSONObject getResponseFromServer(URL url) throws IOException {
        HttpURLConnection connection = ((HttpURLConnection) url.openConnection());
        connection.addRequestProperty("User-Agent", "Mozilla/4.0");
        InputStream input;
        if (connection.getResponseCode() == 200) {
            input = connection.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(input));
            String msg = reader.readLine();
            return new JSONObject(msg);
        } else {
            throw new IOException();
        }
    }

    private boolean handleServerResponse(JSONObject jsonObj) {
        if (!jsonObj.has("id") || !jsonObj.has("title") || !jsonObj.has("imdb_rating") ||
                !NumberUtils.isCreatable(jsonObj.get("imdb_rating").toString()) || !jsonObj.has("overview") ||
                !jsonObj.has("released_on") ) return false;
        String contentId = jsonObj.getString("id");
        if (this.currentUser.getContents().stream().map(Content::getId).collect(Collectors.toList()).contains(contentId)) {
            return false;
        }
        Content newContent = new Content(contentId, jsonObj.getString("title"), jsonObj.getDouble("imdb_rating") );
        this.addNewContent(newContent);
        this.currentContent = newContent;
        return true;
    }

    public void setUserRanking(int rank) {
        if (this.currentContent == null) return;
        this.currentContent.setUserRanking(rank);
        this.currentContent = null;
    }

    private void addNewContent(Content newContent) {
        this.currentUser.addContentToUser(newContent);
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public Content getCurrentContent() {
        return currentContent;
    }

    public void setCurrentContent(Content currentContent) {
        this.currentContent = currentContent;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }
}
