package com.checkmarx.bingewatching;

import com.checkmarx.bingewatching.entities.User;

import java.util.HashSet;
import java.util.Set;

// Data is a singleton class
public class Data {

    private static Data object = new Data();
    private Set<User> userList = new HashSet<>();

    private Data() {};

    public static Data getObject() {
        return object;
    }

    public Set<User> getUserList() {
        return this.userList;
    }

    public boolean addNewUser(User user) {
        return this.userList.add(user);
    }
}
