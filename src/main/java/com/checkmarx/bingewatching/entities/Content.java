package com.checkmarx.bingewatching.entities;

public class Content {

    private String id;
    private String title;
    private Double imdbRating;
    private Integer userRanking;

    public Content(String id, String title, Double imdbRating) {
        this.id = id;
        this.title = title;
        this.imdbRating = imdbRating;
    }

    public String getTitle() {
        return title;
    }

    public Double getImdbRating() {
        return imdbRating;
    }

    public String getId() {
        return id;
    }

    public Integer getUserRanking() {
        return userRanking;
    }

    public void setUserRanking(Integer userRanking) {
        this.userRanking = userRanking;
    }

    @Override
    public String toString() {
        String retValue =  String.format("\ntitle: %s\nuser ranking: %d\n", title, userRanking);
        if (imdbRating != null) {
            retValue += String.format("imdb rating: %.1f\n", imdbRating);
        }
        return retValue;
    }

    @Override
    public boolean equals(Object obj) {
        return (obj.getClass() == Content.class && ((Content) obj).getId().equals(this.id));
    }

    @Override
    public int hashCode() {
        return this.id.hashCode();
    }
}
