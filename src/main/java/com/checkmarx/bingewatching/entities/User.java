package com.checkmarx.bingewatching.entities;

import java.util.ArrayList;
import java.util.List;

public class User {

    private String userId;
    private List<Content> contents = new ArrayList<>();

    public User(String userId) {
        this.userId = userId;
    }

    public void addContentToUser(Content content) {
        this.contents.add(content);
    }

    public String getUserId() {
        return this.userId;
    }

    public List<Content> getContents() {
        return contents;
    }

    public void setContents(List<Content> contents) {
        this.contents = contents;
    }

    @Override
    public boolean equals(Object obj) {
        return (obj.getClass() == User.class && ((User) obj).getUserId().equals(this.userId));
    }

    @Override
    public int hashCode() {
        return this.userId.hashCode();
    }
}
