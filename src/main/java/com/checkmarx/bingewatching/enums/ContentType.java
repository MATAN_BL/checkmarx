package com.checkmarx.bingewatching.enums;

public enum ContentType {
    TV_SHOW,
    MOVIE,
    ANY
}
