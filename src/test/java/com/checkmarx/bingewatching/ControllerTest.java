package com.checkmarx.bingewatching;

import com.checkmarx.bingewatching.entities.Content;
import com.checkmarx.bingewatching.entities.User;
import com.checkmarx.bingewatching.enums.ContentType;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.expectation.PowerMockitoStubber;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.api.mockito.PowerMockito;
import static org.mockito.Mockito.times;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@RunWith(PowerMockRunner.class)
@PrepareForTest(Controller.class)
public class ControllerTest {

    private static final String RESPONSE_ID = "111";
    private static final String RESPONSE_TITLE = "TITLE";
    private static final Double RESPONSE_IMDB = 5.5;
    private static final String RESPONSE_OVERVIEW = "OVERVIEW";
    private static final String RESPONSE_RELEASED_ON = "2000-10-10T00:00:00";
    private static final String USER_ID1 = "1";
    private static final String USER_ID2 = "2";

    private Content content1 = new Content("1", "content_title", 5.5);
    private Content content2 = new Content("2", "content_title", 6.5);
    private Data data;
    private Controller controllerUnderTest;
    private JSONObject response;


    @Before
    public void before() {
        this.data = Data.getObject();
        User user1 = new User(USER_ID1);
        user1.setContents(new ArrayList<>(Arrays.asList(content1)));
        User user2 = new User(USER_ID2);
        user2.setContents(new ArrayList<>(Arrays.asList(content2)));
        this.data.addNewUser(user1);
        this.data.addNewUser(user2);
        response = new JSONObject();
        response.put("id", RESPONSE_ID);
        response.put("title", RESPONSE_TITLE);
        response.put("imdb_rating", RESPONSE_IMDB);
        response.put("overview", RESPONSE_OVERVIEW);
        response.put("released_on", RESPONSE_RELEASED_ON);
        this.controllerUnderTest = PowerMockito.spy(Controller.getObject(this.data));
        this.controllerUnderTest.setCurrentUser(user1);
    }

    /*
     This test checks that method 'getContent' behaves properly for asking content of TV show.
     It checks that 'getResponseFromServer' is called with the correct URL parameter ("show"), and
     that the response is stored for current user. Finally, the test checks that the user can rank the content.
    */
    @Test
    public void getContentTvShowAndRankTest() throws Exception {
        URL url = new URL(String.format(Controller.URL_FORMATTER, "show"));
        PowerMockito.doReturn(response).when(controllerUnderTest, "getResponseFromServer", url);
        controllerUnderTest.getContent(ContentType.TV_SHOW);

        PowerMockito.verifyPrivate(controllerUnderTest, times(1))
            .invoke("readDataFromServer", "show");
        Content currentContent = controllerUnderTest.getCurrentContent();
        Assert.assertEquals(RESPONSE_ID, currentContent.getId());
        Assert.assertEquals(RESPONSE_TITLE, currentContent.getTitle());
        Assert.assertEquals(RESPONSE_IMDB, currentContent.getImdbRating());
        Assert.assertEquals(2, this.controllerUnderTest.getCurrentUser().getContents().size());
        Assert.assertTrue(this.controllerUnderTest.getCurrentUser().getContents().contains(currentContent));
        Assert.assertNull(currentContent.getUserRanking());


    }

    /*
     This test checks the same behaviour as 'getContentTvShowTest', but this time the server retrieves a bad response.
     It checks that 'getResponseFromServer' is called with the correct URL parameter ("show"), and
     that method 'getContent' returns null.
    */
    @Test
    public void getContentTvShowBadResponseTest() throws Exception {
        URL url = new URL(String.format(Controller.URL_FORMATTER, "show"));
        PowerMockito.doThrow(new IOException()).when(controllerUnderTest, "getResponseFromServer", url);
        controllerUnderTest.getContent(ContentType.TV_SHOW);

        PowerMockito.verifyPrivate(controllerUnderTest, times(1))
                .invoke("readDataFromServer", "show");
        PowerMockito.verifyPrivate(controllerUnderTest, times(1))
                .invoke("getResponseFromServer", url);
        Assert.assertEquals(null, controllerUnderTest.getCurrentContent());
    }
    /*
     This test checks method 'setUserRanking'.
    */
    @Test
    public void setUserRankingTest() {
        // check ranking feature
        controllerUnderTest.setCurrentContent(content1);
        controllerUnderTest.setUserRanking(10);
        Assert.assertNull(controllerUnderTest.getCurrentContent());
        Content rankedContent = this.controllerUnderTest.getCurrentUser().getContents().stream().
                filter(content -> content.getId().equals(content1.getId())).collect(Collectors.toList()).get(0);
        Assert.assertEquals(Integer.valueOf(10), rankedContent.getUserRanking());
    }

    /*
     This test checks that 'getContent' method behaves when server retrieves a content that user already watched.
     It makes sure that server is read again and retrieves a new content in this case.
    */
    @Test
    public void getContentTvShowWithRepeatedContent() throws Exception {
        URL url = new URL(String.format(Controller.URL_FORMATTER, "show"));
        JSONObject repeatedResponse = new JSONObject();
        repeatedResponse.put("id", "1");
        ((PowerMockitoStubber) PowerMockito.doReturn(repeatedResponse).doReturn(response))
                .when(controllerUnderTest, "getResponseFromServer", url);

        controllerUnderTest.getContent(ContentType.TV_SHOW);

        PowerMockito.verifyPrivate(controllerUnderTest, times(2))
                .invoke("getResponseFromServer", url);
        Content currentContent = controllerUnderTest.getCurrentContent();
        Assert.assertEquals(RESPONSE_ID, currentContent.getId());
        Assert.assertEquals(RESPONSE_TITLE, currentContent.getTitle());
        Assert.assertEquals(RESPONSE_IMDB, currentContent.getImdbRating());
        Assert.assertEquals(2, this.controllerUnderTest.getCurrentUser().getContents().size());
        Assert.assertTrue(this.controllerUnderTest.getCurrentUser().getContents().contains(currentContent));

    }

    /*
     This test checks that when a user asks for a movie the URL sent to server includes the param "movie"
    */
    @Test
    public void getUrlMovieTest() throws Exception {
        URL url = new URL(String.format(Controller.URL_FORMATTER, "movie"));
        // we mock the method 'getResponseFromServer'
        PowerMockito.doReturn(response).when(controllerUnderTest, "getResponseFromServer", url);
        controllerUnderTest.getContent(ContentType.MOVIE);
        // we check that the method 'getResponseServer' was indeed called
        PowerMockito.verifyPrivate(controllerUnderTest, times(1))
                .invoke("getResponseFromServer", url);
    }

    /*
     this test checks that when a user asks for any content, the URL sent to server includes the param "both"
    */
    @Test
    public void getUrlBothTest() throws Exception {
        URL url = new URL(String.format(Controller.URL_FORMATTER, "both"));
        // we mock the method 'getResponseFromServer'
        PowerMockito.doReturn(response).when(controllerUnderTest, "getResponseFromServer", url);
        controllerUnderTest.getContent(ContentType.ANY);
        // we check that the method 'getResponseServer' was indeed called
        PowerMockito.verifyPrivate(controllerUnderTest, times(1))
                .invoke("getResponseFromServer", url);
    }

    /*
     This test checks the method 'setCurrentUserId'. It checks that when a user ID that doesn't exist in data is inserted,
      a new user entity is created in data.
    */
    @Test
    public void setCurrentUserIdTest() {
        final String NEW_USER_ID = "3";
        Assert.assertEquals(2, this.data.getUserList().size());
        boolean retValue = this.controllerUnderTest.setCurrentUserId("2");
        Assert.assertEquals(2, this.data.getUserList().size());
        Assert.assertTrue(retValue);
        retValue = this.controllerUnderTest.setCurrentUserId("3");
        Assert.assertEquals(3, this.data.getUserList().size());
        Assert.assertTrue(this.data.getUserList().stream().filter(user -> user.getUserId().equals(NEW_USER_ID))
                .collect(Collectors.toList()).size() == 1);
        Assert.assertTrue(this.controllerUnderTest.getCurrentUser().getUserId().equals(NEW_USER_ID));
        Assert.assertFalse(retValue);
    }
    /*
     This test checks that method 'getHistory' returns the content of user1 (the default current user),
     then change the current user to user2 and check that now 'getHistory' returns the content of user2.
    */
    @Test
    public void getHistoryTest() {
        List<Content> history = this.controllerUnderTest.getHistory();
        Assert.assertEquals(1, history.size());
        Assert.assertEquals(content1, history.get(0));
        this.controllerUnderTest.setCurrentUserId(USER_ID2);
        // now change user and check if history changed
        history = this.controllerUnderTest.getHistory();
        Assert.assertEquals(1, history.size());
        Assert.assertEquals(content2, history.get(0));
    }
}
